sensor	period	  duty	load %
A		100 ms	 20 ms	20%		TIM1
B		150 ms	 40 ms	27%		TIM15
C		350 ms	100 ms	29%		TIM16
----------------------------
						76% < 100%

statische Vergabe fallender Prioritäten bei steigender Periodendauer: Rate Monotonic Scheduling (RMS)
