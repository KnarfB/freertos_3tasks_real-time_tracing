/*
 * tracer.h
 *
 *  Created on: Jan 10, 2023
 *      Author: live
 */

#ifndef INC_TRACER_H_
#define INC_TRACER_H_

#include <stdio.h>

static void tracer_init() {
	static int init;
	if(init)
		return;
	init = 1;
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	ITM->LAR = 0xC5ACCE55;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
	DWT->CYCCNT = 0;
	printf("%6lu CC %lu\n", DWT->CYCCNT, SystemCoreClock);
}

__STATIC_INLINE void tracer_TASK_CREATE(  uint32_t uxTaskNumber, char* taskName ) {
	tracer_init();
	printf("%6lu TC %lu %s\n", DWT->CYCCNT, uxTaskNumber, taskName);
}

__STATIC_INLINE void tracer_TASK_SWITCHED_IN( uint32_t uxTaskNumber ) {
	printf("%6lu TI %lu\n", DWT->CYCCNT, uxTaskNumber);
}

__STATIC_INLINE void tracer_TASK_SWITCHED_OUT( uint32_t uxTaskNumber ) {
	printf("%6lu TO %lu\n", DWT->CYCCNT, uxTaskNumber);
}

#define traceTASK_CREATE(pxNewTCB) 	tracer_TASK_CREATE(pxNewTCB->uxTCBNumber, pxNewTCB->pcTaskName);
#define traceTASK_SWITCHED_IN() 	tracer_TASK_SWITCHED_IN(pxCurrentTCB->uxTCBNumber);
#define traceTASK_SWITCHED_OUT() 	tracer_TASK_SWITCHED_OUT(pxCurrentTCB->uxTCBNumber);

#endif /* INC_TRACER_H_ */
